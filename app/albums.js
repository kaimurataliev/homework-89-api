const express = require('express');
const Album = require('../models/Album');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const router = express.Router();

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    // Album get
    router.get('/', (req, res) => {
        if(req.query.artist) {
            Album.find({performer: req.query.artist}).populate('performer')
                .then(album => {
                    res.send(album)
                })
                .catch(err => res.status(404).send({error: 'something wrong happened'}))
        } else {
            Album.find().populate('performer')
                .then(album => res.send(album))
                .catch(() => res.sendStatus(500))
        }

    });

    router.get('/:id', (req, res) => {
        const id = req.params.id;
        Album.findOne({_id: id}, (error, result) => {
            if(error) {
                res.status(400).send(error.message)
            } else {
                res.send(result)
            }
        })
    });

    // Album post
    router.post('/', upload.single('image') ,(req, res) => {
        const albumData = req.body;

        if (req.file) {
            albumData.image = req.file.filename;
        } else {
            albumData.image = null;
        }

        const album = new Album(albumData);

        album.save()
            .then(result => res.send(result))
            .catch(error => res.status(400).send(error));
    });

    return router;
};

module.exports = createRouter;