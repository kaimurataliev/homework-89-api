const mongoose = require('mongoose');
const config = require('./config');

const Album = require('./models/Album');
const User = require('./models/User');
const Artist = require('./models/Artist');
const Track = require('./models/Track');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('tracks');
        await db.dropCollection('users');
        await db.dropCollection('albums');
        await db.dropCollection('artists');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [user, admin] = await User.create(
        {
            username: 'user',
            password: '123',
            role: 'user'
        },
        {
            username: 'admin',
            password: '123',
            role: 'admin'
        }
    );

    const [artist1, artist2, artist3] = await Artist.create(
        {
            name: 'artist1',
            image: 'LcdxNYHEof99rXBywI1qv.jpg',
            information: 'some info 1'
        },
        {
            name: 'artist2',
            image: 'Rx4caBNzxy1yLdB6XS8dM.jpg',
            information: 'some info 2'
        },
        {
            name: 'artist3',
            image: 'R0Bul3tEpgNTiqmiwK4ew.jpg',
            information: 'some info 3'
        }
    );

    const [album1, album2, album3] = await Album.create(
        {
            title: 'album1',
            performer: artist1._id,
            year: '2016',
            image: 'TfWcBNpNSchdt_0LVCl6d.jpg'
        },
        {
            title: 'album2',
            performer: artist2._id,
            year: '2016',
            image: 'TfWcBNpNSchdt_0LVCl6d.jpg'
        },
        {
            title: 'album3',
            performer: artist3._id,
            year: '2016',
            image: 'TfWcBNpNSchdt_0LVCl6d.jpg'
        }
    );

    await Track.create({
            number: '1',
            title: 'track 1',
            album: album1._id,
            long: 4
        },
        {
            number: '2',
            title: 'track 2',
            album: album1._id,
            long: 4
        },
        {
            number: '3',
            title: 'track 3',
            album: album1._id,
            long: 4
        },
        {
            number: '4',
            title: 'track 4',
            album: album1._id,
            long: 4
        },
        {
            number: '5',
            title: 'track 5',
            album: album1._id,
            long: 4
        },
        {
            number: '1',
            title: 'track 1',
            album: album2._id,
            long: 4
        },
        {
            number: '2',
            title: 'track 2',
            album: album2._id,
            long: 4
        },
        {
            number: '3',
            title: 'track 3',
            album: album2._id,
            long: 4
        },
        {
            number: '4',
            title: 'track 4',
            album: album2._id,
            long: 4
        },
        {
            number: '5',
            title: 'track 5',
            album: album2._id,
            long: 4
        },
        {
            number: '1',
            title: 'track 1',
            album: album3._id,
            long: 4
        },
        {
            number: '2',
            title: 'track 2',
            album: album3._id,
            long: 4
        },
        {
            number: '3',
            title: 'track 3',
            album: album3._id,
            long: 4
        },
        {
            number: '4',
            title: 'track 4',
            album: album3._id,
            long: 4
        },
        {
            number: '5',
            title: 'track 5',
            album: album3._id,
            long: 4
        }
    );

    db.close();
});