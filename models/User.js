const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const SALT_FACTOR = 10;
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String, required: true, unique: true
    },
    password: {
        type: String, required: true
    },
    role: {
        type: String, default: 'user', enum: ['admin', 'user']
    },
    token: String
});

UserSchema.pre('save', async function (next) {
    if(!this.isModified('password')) return next();
    const salt = await bcrypt.genSaltSync(SALT_FACTOR);
    this.password = await bcrypt.hashSync(this.password, salt);
    next();
});

UserSchema.set('toJSON', {
    transform: (doc, ret) => {
        delete ret.password;
        return ret;
    }
});

UserSchema.methods.checkPassword = function (password) {
    return bcrypt.compare(password, this.password)
};

const User = mongoose.model('User', UserSchema);

module.exports = User;