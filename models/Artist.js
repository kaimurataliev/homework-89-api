const mongoose = require('mongoose');

const ArtistSchema = new mongoose.Schema({
    name: {
        type: String, required: true
    },
    image: {
        type: String
    },
    information: {
        type: String, required: true
    },
    status: {
        type: String, default: 'not published', enum: ['published', 'not published']
    }
});

const Artist = mongoose.model('Artist', ArtistSchema);

module.exports = Artist;