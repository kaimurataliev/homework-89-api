const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    title: {
        type: String, required: true
    },
    performer: {
        type: Schema.Types.ObjectId, ref: 'Artist', required: true
    },
    year: {
        type: Number, required: true
    },
    image: {
        type: String
    },
    status: {
        type: String, default: 'not published', enum: ['published', 'not published']
    }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;